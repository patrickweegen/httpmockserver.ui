# HttpMockServer #

HttpMockServer.UI provides a UI for [HttpMockServer](https://bitbucket.org/patrickweegen/httpmockserver). Could be used for manually testing with faked HTTP-API

## How to build / contribute
* Clone
* Run `dotnet build`
* Create a topic branch
* Implement new feature / improvment / bugfix and tests
* Send a pull request