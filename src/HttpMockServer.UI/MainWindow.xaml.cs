﻿using System.ComponentModel;
using System.Windows;
using HttpMockServer.UI.ViewModels;
using HttpMockServer.UI.Views;
using Serilog;

namespace HttpMockServer.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private static readonly ILogger Log = Serilog.Log.ForContext<MainWindow>();
        private MainViewModel _mainViewModel;

        public MainWindow()
        {
            Log.Verbose("Initializing ...");
               
            InitializeComponent();

            Loaded += OnLoaded;

            Log.Verbose("Initialized");
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            Log.Verbose("Handling Loaded event ...");
            Loaded -= OnLoaded;

            var view = new MainView();
            _mainViewModel = new MainViewModel();
            view.DataContext = _mainViewModel;

            Content = view;
            Log.Verbose("Handled Loaded event");
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            Log.Verbose("Handling Closing event ...");

            base.OnClosing(e);
            _mainViewModel?.Dispose();

            Log.Verbose("Handled Closing event ...");
        }
    }
}
