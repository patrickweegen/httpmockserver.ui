﻿using System.Windows.Input;

namespace HttpMockServer.UI.Utils.Commands
{
    internal interface IActionCommand : ICommand
    {
        void RefreshCanExecute();
    }
}