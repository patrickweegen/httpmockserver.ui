﻿using System;
using HttpMockServer.UI.Annotations;

namespace HttpMockServer.UI.Utils.Commands
{
    internal sealed class ActionCommand : ActionCommand<object>
    {
        public ActionCommand([NotNull] Action execute, [CanBeNull] Func<bool> canExecute = null)
            : base(o => execute(), canExecute == null ? (Func<object, bool>) null : o => canExecute())
        { }
    }

    internal class ActionCommand<T> : IActionCommand
    {
        public event EventHandler CanExecuteChanged;
        private readonly Action<T> _execute;
        [CanBeNull] private readonly Func<T, bool> _canExecute;

        public ActionCommand([NotNull] Action<T> execute, [CanBeNull] Func<T, bool> canExecute = null)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            if (_canExecute != null) return _canExecute((T) parameter);
            return true;
        }

        public void Execute(object parameter)
        {
            _execute((T) parameter);
        }

        public void RefreshCanExecute()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}