﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using HttpMockServer.UI.Annotations;
using HttpMockServer.UI.Utils.Commands;
using Serilog;

namespace HttpMockServer.UI.ViewModels
{
    internal sealed class MainViewModel : INotifyPropertyChanged,
        IDisposable
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private static readonly ILogger Log = Serilog.Log.ForContext<MainViewModel>();
        private MockServer _server;
        private bool _selectPortAutomaticly = true;
        private int _port;

        public MainViewModel()
        {
            Log.Verbose("Initializing ...");

            CreateCommand = new ActionCommand(CreateMockServer, () => _server == null);
            StartCommand = new ActionCommand(StartMockServer, ()=> _server != null);
            DestroyCommand = new ActionCommand(DestroyMockServer, ()=> _server != null);

            Log.Verbose("Initialized");
        }

        public IActionCommand CreateCommand { get; }
        public IActionCommand StartCommand { get; }
        public IActionCommand DestroyCommand { get; }

        public bool SelectPortAutomaticly
        {
            get => _selectPortAutomaticly;
            set
            {
                if (value == _selectPortAutomaticly) return;
                _selectPortAutomaticly = value;
                OnPropertyChanged(nameof(SelectPortAutomaticly));
                OnPropertyChanged(nameof(CanSwitchPortManually));
            }
        }

        [NonNegativeValue]
        public int Port
        {
            get => _port;
            set
            {
                if (value == _port) return;
                _port = value;
                OnPropertyChanged(nameof(Port));
            }
        }

        public bool CanSwitchPortManually => !SelectPortAutomaticly && _server == null;
        public bool CanSwitchAutomaticPortSelection => _server == null;

        private void CreateMockServer()
        {
            Log.Debug("Creating mockserver ...");

            if (SelectPortAutomaticly)
            {
                _server = MockServer.Create();
                Port = _server.Port;
            }
            else
            {
                _server = MockServer.Create(Port);
            }

            UpdateUiState();

            Log.Information("Mockserver created, listening on port {PortNumber}", _server.Port);
        }

        private void StartMockServer()
        {
            if (_server == null) return;

            Log.Debug("Starting mockserver ...");

            _server.Start();
            UpdateUiState();

            Log.Information("Started mockserver");

        }

        private void DestroyMockServer()
        {
            if (_server == null) return;

            Log.Debug("Destroying mockserver ...");
            _server.Dispose();
            _server = null;

            UpdateUiState();

            Log.Information("Destroyed Mockserver");
        }

        private void UpdateUiState()
        {
            CreateCommand.RefreshCanExecute();
            StartCommand.RefreshCanExecute();
            DestroyCommand.RefreshCanExecute();

            OnPropertyChanged(nameof(CanSwitchPortManually));
            OnPropertyChanged(nameof(CanSwitchAutomaticPortSelection));
        }

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Dispose()
        {
            Log.Verbose("Disposing ...");

            _server?.Dispose();

            Log.Verbose("Disposed");
        }
    }
}