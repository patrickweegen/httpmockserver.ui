﻿using System.Windows.Controls;
using Serilog;

namespace HttpMockServer.UI.Views
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : UserControl
    {
        private static readonly ILogger Log = Serilog.Log.ForContext<MainView>();
        
        public MainView()
        {
            Log.Verbose("Initilizing MainView ...");
            
            InitializeComponent();

            Log.Verbose("Initilized MainView");
        }
    }
}
