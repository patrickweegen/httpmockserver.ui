﻿using System;
using System.Configuration;
using System.Windows;
using Serilog;
using Serilog.Core;
using Serilog.Events;

namespace HttpMockServer.UI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var logPath = ConfigurationManager.AppSettings["logPath"];

            var logLevelValue = ConfigurationManager.AppSettings["logLevel"];
            if (!Enum.TryParse<LogEventLevel>(logLevelValue, out var level))
            {
                level = LogEventLevel.Debug;
            }
            Log.Logger = new LoggerConfiguration()
                         .Enrich.FromLogContext()
                         .WriteTo.File(logPath, rollingInterval: RollingInterval.Day, rollOnFileSizeLimit: true,
                             outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level}] [{SourceContext}] {Message:lj}{NewLine}{Exception}", retainedFileCountLimit: 5)
                         .MinimumLevel.Is(level)
                         .CreateLogger();

            Log.ForContext<App>()
               .Information("Logging intialized with level {level}", level);
        }
    }
}